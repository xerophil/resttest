package demo;

import demo.model.GreetingRepository;
import demo.model.Greeting;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RestTestApplication.class)
@WebAppConfiguration
public class RestTestApplicationTests {

    private List<Greeting> greetingList = new ArrayList<>();
    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private GreetingRepository greetingRepository;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
                hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        greetingList.addAll(greetingRepository.findAll());
//        greetingRepository.deleteAllInBatch();
//
//        greetingList.add(greetingRepository.save(new Greeting()));
//        greetingList.add(greetingRepository.save(new Greeting("hans")));
//        greetingList.add(greetingRepository.save(new Greeting("franz")));
    }

    @Test
    public void greetingNotFound() throws Exception {
        mockMvc.perform(get("/greetings/4").contentType(contentType)
        ).andExpect(status().isNotFound());

    }

    @Test
    public void readSingleGreeting() throws Exception {
        mockMvc.perform(get("/greetings/"+greetingList.get(0).getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].id", Matchers.is(greetingList.get(0).getId())))
                .andExpect(jsonPath("$[0].content", Matchers.is(greetingList.get(0).getContent())));        
    }

    @Test
    public void createGreeting() throws Exception {
        mockMvc.perform(post("/greetings?name=hannes")
                .contentType(contentType))
                .andExpect(status().isCreated());
        
    }

}
