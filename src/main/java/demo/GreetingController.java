package demo;

import demo.model.GreetingRepository;
import demo.model.Greeting;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/greetings")
public class GreetingController {

    @Autowired
    GreetingRepository greetingRepository;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Greeting> addGreeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        Greeting g = new Greeting(name);
        greetingRepository.save(g);
        return new ResponseEntity(g, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Greeting> getGreetings() {
        return greetingRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Greeting> deleteGreeting(@PathVariable("id") Long id) {
        Greeting g = greetingRepository.findOne(id);
        if (g == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        greetingRepository.delete(g);
        return new ResponseEntity(g, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Greeting> getGreeting(@PathVariable("id") Long id) {
        Greeting g = greetingRepository.findOne(id);
        if (g == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(g, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Greeting> updateGreeting(
            @PathVariable("id") Long id,
            @RequestParam(value = "name", defaultValue = "World") String name) {
        Greeting g = greetingRepository.findOne(id);
        if (g == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        g.setContent(name);
        greetingRepository.save(g);
        return new ResponseEntity(g, HttpStatus.OK);
    }
}
