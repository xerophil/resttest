package demo;

import demo.model.Account;
import demo.model.AccountRepository;
import demo.model.Bookmark;
import demo.model.BookmarkRepository;
import demo.model.GreetingRepository;
import java.io.IOException;
import java.util.Arrays;
import javax.servlet.FilterChain;
import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class RestTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestTestApplication.class, args);
    }
}

@Component
class BookmarkCommandLineRunner implements CommandLineRunner {

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    BookmarkRepository bookmarkRepository;

    @Override
    public void run(String... args) throws Exception {
        Arrays.asList(
                "jhoeller,dsyer,pwebb,ogierke,rwinch,mfisher,mpollack,jlong".split(","))
                .forEach(
                        a -> {
                            Account account = accountRepository.save(new Account(a,
                                            "password"));
                            bookmarkRepository.save(new Bookmark(account,
                                            "http://bookmark.com/1/" + a, "A description"));
                            bookmarkRepository.save(new Bookmark(account,
                                            "http://bookmark.com/2/" + a, "A description"));
                        });
    }

}

@Component
class GreetingCommandLineRunnder implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Initial Greetings:");
        greetingRepository.findAll().stream().forEach((g) -> {
            System.out.println(g.toString());
        });
    }
    @Autowired
    GreetingRepository greetingRepository;

}

@Component
class SimpleCORSFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with, Content-Type");
        chain.doFilter(req, res);
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }

}
