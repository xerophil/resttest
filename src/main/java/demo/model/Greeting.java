package demo.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;


@Entity
public class Greeting implements Serializable{

    @Transient
    private static final String template = "Hello, %s!";
    
    @Id @GeneratedValue
    private long id;
    private String content;

    public Greeting() {
    }
    
    public Greeting(String content) {
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return String.format(template, content);
    }

    public void setContent(String content) {
        this.content=content;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Greeting other = (Greeting) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Greeting{" + "id=" + id + ", content=" + content + '}';
    }
    
    

}
