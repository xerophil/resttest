/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo.model;

import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author sibe
 */
public interface GreetingRepository extends JpaRepository<Greeting, Long>{
    Collection<Greeting> findByContent(String content);
}
